### Init 

```php
$configuration = new \K0nias\ZohoApi\Configuration(__DIR__ . '/../');
$client = new \K0nias\ZohoApi\ZohoCRMClient($configuration, 'Contacts');
```

#### Get records

```php
$records = $client->getRecords()
    ->page(1)
    ->perPage(1)
    ->selectColumns(['Email'])
    ->sortBy('Email')
    ->sortDirection('DESC')
    ->since(new \DateTimeImmutable('2018-09-18T11:14:50-07:00'))
    ->send();
```

#### Get record

```php
$client->getRecord('1234')
    ->send();
```

#### Search records

```php
$records = $client->searchRecords()
    ->page(1)
    ->perPage(1)
    ->email("email@email.com")
    // OR ->criteria([])
    // OR ->phone('123456789')
    // OR ->word('word')
    ->send();
```

#### Create and update record

```php

$record = \ZCRMRecord::getInstance('Contacts', null);

$client->createRecord($record)
    ->send();
    
$record = \ZCRMRecord::getInstance('Contacts', '12345');
    
$client->updateRecord($record)
    ->send();
```

#### Delete record

```php

$record = \ZCRMRecord::getInstance('Contacts', null);

$client->deleteRecord('12345')
    ->send();
    
```

#### Create and update records

```php

$record = \ZCRMRecord::getInstance('Contacts', null);

$client->createRecords([$record])
    ->send();
    
$record = \ZCRMRecord::getInstance('Contacts', '12345');
    
$client->updateRecords[$record])
    ->send();
```

#### Delete records

```php

$record = \ZCRMRecord::getInstance('Contacts', null);

$client->deleteRecords(['12345'])
    ->send();
    
```
