<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests;

use K0nias\ZohoApi\Configuration;
use PHPUnit\Framework\TestCase;

class ConfigurationTest extends TestCase
{

    public function testLoadToArray(): void
    {
        $configuration = new Configuration(__DIR__ . '/resources/configuration/both');

        $config = $configuration->loadToArray();

        self::assertArrayHasKey('applicationLogFilePath', $config);
        self::assertArrayHasKey('client_id', $config);
    }

    public function testMissingOauthConfiguration(): void
    {
        $configuration = new Configuration(__DIR__ . '/resources/configuration/configuration_only');

        $this->expectException(\Throwable::class);
        $this->expectExceptionMessage(
            sprintf(
                'Missing or invalid configuration file "%s/resources/configuration/configuration_only/oauth_configuration.properties"',
                __DIR__
            )
        );

        $configuration->loadToArray();
    }

    public function testMissingConfiguration(): void
    {
        $configuration = new Configuration(__DIR__ . '/resources/configuration/oauth_configuration_only');

        $this->expectException(\Throwable::class);
        $this->expectExceptionMessage(
            sprintf(
                'Missing or invalid configuration file "%s/resources/configuration/oauth_configuration_only/configuration.properties"',
                __DIR__
            )
        );

        $configuration->loadToArray();
    }

}
