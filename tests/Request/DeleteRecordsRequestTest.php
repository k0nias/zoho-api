<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\DeleteRecordsRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class DeleteRecordsRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $client->expects(self::once())
            ->method('doDeleteRecords')
            ->with(
                'someModuleName',
                ['1111']
            );

        $request = new DeleteRecordsRequest($client, 'someModuleName', ['1111']);
        $request->send();

    }

}
