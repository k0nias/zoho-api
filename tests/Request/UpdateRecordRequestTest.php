<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\UpdateRecordRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class UpdateRecordRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $record = \ZCRMRecord::getInstance('Invoices', '1231312');

        $record->setFieldValue('Subject', 'Iphone sale to John');

        $client->expects(self::once())
            ->method('doUpdateRecord')
            ->with($record);

        $request = new UpdateRecordRequest($client, $record);

        $request->send();
    }

}
