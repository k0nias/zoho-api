<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\DeleteRecordRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class DeleteRecordRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $recordId = '12345679';

        $client->expects(self::once())
            ->method('doDeleteRecord')
            ->with(
                'someModuleName',
                $recordId
            );

        $request = new DeleteRecordRequest($client, 'someModuleName', $recordId);

        $request->send();
    }

}
