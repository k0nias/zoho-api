<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\GetRecordRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class GetRecordRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $recordId = '12345679';

        $client->expects(self::once())
            ->method('doGetRecord')
            ->with(
                'someModuleName',
                $recordId
            );

        $request = new GetRecordRequest($client, 'someModuleName', $recordId);

        $request->send();
    }

}
