<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use DateTimeImmutable;
use K0nias\ZohoApi\Request\GetRecordsRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class GetRecordsRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $since = new DateTimeImmutable('2018-10-22 09:15:00');

        $client->expects(self::once())
            ->method('doGetRecords')
            ->with(
                'someModuleName',
                ['Email', 'Company'],
                'Name',
                'asc',
                1,
                100,
                $since
            );

        $request = new GetRecordsRequest($client, 'someModuleName');

        $request->selectColumns(['Email', 'Company'])
            ->sortBy('Name')
            ->sortDirection('ASC')
            ->page(1)
            ->perPage(100)
            ->since($since);

        $request->send();
    }

}
