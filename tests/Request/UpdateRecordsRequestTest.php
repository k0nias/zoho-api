<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\UpdateRecordsRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class UpdateRecordsRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $record = \ZCRMRecord::getInstance('Invoices', '1231312');

        $record->setFieldValue('Subject', 'Iphone sale to John');

        $client->expects(self::once())
            ->method('doUpdateRecords')
            ->with(
                'someModuleName',
                [$record]
            );

        $request = new UpdateRecordsRequest($client, 'someModuleName', [$record]);

        $request->send();
    }

}
