<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\CreateRecordsRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class CreateRecordsRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $record = \ZCRMRecord::getInstance('Invoices', null);

        $record->setFieldValue('Subject', 'Iphone sale to John');

        $client->expects(self::once())
            ->method('doCreateRecords')
            ->with(
                'someModuleName',
                [$record]
            );

        $request = new CreateRecordsRequest($client, 'someModuleName', [$record]);

        $request->send();
    }

}
