<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\CreateRecordRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class CreateRecordRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $record = \ZCRMRecord::getInstance('Invoices', null);

        $record->setFieldValue('Subject', 'Iphone sale to John');

        $client->expects(self::once())
            ->method('doCreateRecord')
            ->with($record);

        $request = new CreateRecordRequest($client, $record);

        $request->send();
    }

}
