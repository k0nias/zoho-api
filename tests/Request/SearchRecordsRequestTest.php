<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Tests\Request;

use K0nias\ZohoApi\Request\SearchRecordsRequest;
use K0nias\ZohoApi\ZohoCRMClient;
use PHPUnit\Framework\TestCase;

class SearchRecordsRequestTest extends TestCase
{

    public function testRequest(): void
    {
        $client = $this->createMock(ZohoCRMClient::class);

        $client->expects(self::once())
            ->method('doSearchRecords')
            ->with(
                'someModuleName',
                ['criteria' => '12'],
                'email@email.cz',
                '123456789',
                'someWord',
                2,
                300
            );

        $request = new SearchRecordsRequest($client, 'someModuleName');

        $request->criteria(['criteria' => '12'])
            ->email('email@email.cz')
            ->phone('123456789')
            ->word('someWord')
            ->page(2)
            ->perPage(300);

        $request->send();
    }

}
