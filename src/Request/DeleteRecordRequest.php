<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use APIResponse;
use K0nias\ZohoApi\ZohoCRMClient;

/**
 * @see https://www.zoho.com/crm/help/api/v2/#update-specify-records
 */
class DeleteRecordRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $recordId;
    /**
     * @var string
     */
    private $module;

    public function __construct(ZohoCRMClient $client, string $module, string $recordId)
    {
        $this->client = $client;
        $this->recordId = $recordId;
        $this->module = $module;
    }

    public function send(): APIResponse
    {
        return $this->client->doDeleteRecord(
            $this->module,
            $this->recordId
        );
    }

}
