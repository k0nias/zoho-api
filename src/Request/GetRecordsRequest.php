<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use DateTimeImmutable;
use K0nias\ZohoApi\ZohoCRMClient;
use ZCRMRecord;

/**
 * @see https://www.zoho.com/crm/help/api/v2/#ra-search-records
 */
class GetRecordsRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $module;
    /**
     * @var string[]|null
     */
    private $selectColumns = null;
    /**
     * @var string|null
     */
    private $sortBy = null;
    /**
     * @var string|null
     */
    private $sortDirection = null;
    /**
     * @var int|null
     */
    private $page = null;
    /**
     * @var int|null
     */
    private $perPage = null;
    /**
     * @var \DateTimeImmutable|null
     */
    private $since = null;

    public function __construct(ZohoCRMClient $client, string $module)
    {
        $this->client = $client;
        $this->module = $module;
    }

    /**
     * @return ZCRMRecord[]
     */
    public function send(): array
    {
        return $this->client->doGetRecords(
            $this->module,
            $this->selectColumns,
            $this->sortBy,
            $this->sortDirection,
            $this->page,
            $this->perPage,
            $this->since
        );
    }

    public function selectColumns(array $columns): self
    {
        $this->selectColumns = $columns;

        return $this;
    }

    public function page(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function perPage(int $perPage): self
    {
        $this->perPage = $perPage;

        return $this;
    }

    public function sortBy(string $column): self
    {
        $this->sortBy = $column;

        return $this;
    }

    public function sortDirection(string $direction): self
    {
        $this->sortDirection = strtolower($direction);

        return $this;
    }

    public function since(DateTimeImmutable $since): self
    {
        $this->since = $since;

        return $this;
    }

}
