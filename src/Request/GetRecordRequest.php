<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use K0nias\ZohoApi\ZohoCRMClient;
use ZCRMRecord;

/**
 * @see https://www.zoho.com/crm/help/api/v2/#single-records
 */
class GetRecordRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $module;
    /**
     * @var string
     */
    private $recordId;

    public function __construct(ZohoCRMClient $client, string $module, string $recordId)
    {
        $this->client = $client;
        $this->module = $module;
        $this->recordId = $recordId;
    }

    public function send(): ZCRMRecord
    {
        return $this->client->doGetRecord(
            $this->module,
            $this->recordId
        );
    }

}
