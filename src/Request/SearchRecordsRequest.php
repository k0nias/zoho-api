<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use K0nias\ZohoApi\ZohoCRMClient;
use ZCRMRecord;

/**
 * @see https://www.zoho.com/crm/help/api/v2/#ra-search-records
 */
class SearchRecordsRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $module;
    /**
     * @var string[]|null
     */
    private $criteria = null;
    /**
     * @var string|null
     */
    private $email = null;
    /**
     * @var string|null
     */
    private $phone = null;
    /**
     * @var string|null
     */
    private $word  = null;
    /**
     * @var int|null
     */
    private $page = null;
    /**
     * @var int|null
     */
    private $perPage = null;

    public function __construct(ZohoCRMClient $client, string $module)
    {
        $this->client = $client;
        $this->module = $module;
    }

    /**
     * @return ZCRMRecord[]
     */
    public function send(): array
    {
        return $this->client->doSearchRecords(
            $this->module,
            $this->criteria,
            $this->email,
            $this->phone,
            $this->word,
            $this->page,
            $this->perPage
        );
    }


    public function criteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }

    public function email(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function phone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function word(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function page(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function perPage(int $perPage): self
    {
        $this->perPage = $perPage;

        return $this;
    }

}
