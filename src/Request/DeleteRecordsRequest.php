<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use K0nias\ZohoApi\ZohoCRMClient;

class DeleteRecordsRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $module;
    /**
     * @var string[]
     */
    private $recordIds;

    /**
     * @param string[] $recordIds
     */
    public function __construct(ZohoCRMClient $client, string $module, array $recordIds)
    {
        $this->client = $client;
        $this->module = $module;
        $this->recordIds = $recordIds;
    }

    /**
     * @return \EntityResponse[]
     */
    public function send(): array
    {
        return $this->client->doDeleteRecords(
            $this->module,
            $this->recordIds
        );
    }

}
