<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use APIResponse;
use K0nias\ZohoApi\ZohoCRMClient;
use ZCRMRecord;

/**
 * @see https://www.zoho.com/crm/help/api/v2/#create-specify-records
 */
class CreateRecordRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var \ZCRMRecord
     */
    private $record;

    public function __construct(ZohoCRMClient $client, ZCRMRecord $record)
    {
        $this->client = $client;
        $this->record = $record;
    }

    public function send(): APIResponse
    {
        return $this->client->doCreateRecord(
            $this->record
        );
    }

}
