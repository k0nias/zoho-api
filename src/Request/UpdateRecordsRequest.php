<?php declare(strict_types = 1);

namespace K0nias\ZohoApi\Request;

use K0nias\ZohoApi\ZohoCRMClient;

class UpdateRecordsRequest
{

    /**
     * @var ZohoCRMClient
     */
    private $client;
    /**
     * @var string
     */
    private $module;
    /**
     * @var \ZCRMRecord[]
     */
    private $records;

    /**
     * @param \ZCRMRecord[] $records
     */
    public function __construct(ZohoCRMClient $client, string $module, array $records)
    {
        $this->client = $client;
        $this->module = $module;
        $this->records = $records;
    }

    /**
     * @return \EntityResponse[]
     */
    public function send(): array
    {
        return $this->client->doUpdateRecords(
            $this->module,
            $this->records
        );
    }

}
