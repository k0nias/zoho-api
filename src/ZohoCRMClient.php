<?php declare(strict_types = 1);

namespace K0nias\ZohoApi;

use APIResponse;
use DateTimeImmutable;
use K0nias\ZohoApi\Request\CreateRecordRequest;
use K0nias\ZohoApi\Request\CreateRecordsRequest;
use K0nias\ZohoApi\Request\DeleteRecordRequest;
use K0nias\ZohoApi\Request\DeleteRecordsRequest;
use K0nias\ZohoApi\Request\GetRecordRequest;
use K0nias\ZohoApi\Request\GetRecordsRequest;
use K0nias\ZohoApi\Request\SearchRecordsRequest;
use K0nias\ZohoApi\Request\UpdateRecordRequest;
use K0nias\ZohoApi\Request\UpdateRecordsRequest;
use MassEntityAPIHandler;
use ZCRMModule;
use ZCRMRecord;
use ZCRMRestClient;

class ZohoCRMClient
{

    /**
     * @var \K0nias\ZohoApi\Configuration
     */
    private $configuration;
    /**
     * @var string
     */
    private $module;

    public function __construct(Configuration $configuration, string $module)
    {
        $this->configuration = $configuration;
        $this->module = $module;

        ZCRMRestClient::initialize($configuration->loadToArray());
    }

    public function setModule(string $module): void
    {
        $this->module = $module;
    }

    public function doCreateRecord(ZCRMRecord $record): APIResponse
    {
        /** @var \APIResponse $response */
        $response = $record->create();

        return $response;
    }

    public function doUpdateRecord(ZCRMRecord $record): APIResponse
    {
        /** @var \APIResponse $response */
        $response = $record->update();

        return $response;
    }

    public function doDeleteRecord(string $moduleName, string $recordId): APIResponse
    {
        $record = ZCRMRecord::getInstance($moduleName, $recordId);

        return $record->delete();
    }

    public function doGetRecord(string $moduleName, string $recordId): ZCRMRecord
    {
        $module = ZCRMModule::getInstance($moduleName);

        /** @var \APIResponse $response */
        $response = $module->getRecord($recordId);

        return $response->getData();
    }

    /**
     * @param \ZCRMRecord[] $records
     *
     * @return \EntityResponse[]
     */
    public function doCreateRecords(string $moduleName, array $records): array
    {
        foreach ($records as $record) {
            if ( ! $record instanceof ZCRMRecord) {
                throw new \Exception(sprintf('Records parameter to create must be all instances of %s', ZCRMRecord::class));
            }
        }

        $module = ZCRMModule::getInstance($moduleName);

        /** @var \BulkAPIResponse $response */
        $response = $module->createRecords($records);

        return $response->getEntityResponses();
    }

    /**
     * @param \ZCRMRecord[] $records
     *
     * @return \EntityResponse[]
     */
    public function doUpdateRecords(string $moduleName, array $records): array
    {
        foreach ($records as $record) {
            if ( ! $record instanceof ZCRMRecord) {
                throw new \Exception(sprintf('Records parameter to update must be all instances of %s', ZCRMRecord::class));
            }
        }

        $module = ZCRMModule::getInstance($moduleName);

        /** @var \BulkAPIResponse $response */
        $response = $module->updateRecords($records);

        return $response->getEntityResponses();
    }

    /**
     * @param string[] $recordIds
     *
     * @return \EntityResponse[]
     */
    public function doDeleteRecords(string $moduleName, array $recordIds): array
    {
        $module = ZCRMModule::getInstance($moduleName);

        /** @var \BulkAPIResponse $response */
        $response = $module->deleteRecords($recordIds);

        return $response->getEntityResponses();
    }

    /**
     * @return ZCRMRecord[]
     */
    public function doGetRecords(
        string $moduleName,
        ?array $selectColumns = null,
        ?string $sortByField = null,
        ?string $sortDirection = null,
        ?int $page = null,
        ?int $perPage = null,
        ?DateTimeImmutable $since = null
    ): array
    {
        $module = ZCRMModule::getInstance($moduleName);
        $handler = MassEntityAPIHandler::getInstance($module);

        if ($selectColumns !== null) {
            $handler->addParam('fields', implode(',', $selectColumns));
        }

        $headers = [];

        if ($since !== null) {
            $headers['If-Modified-Since'] = $since->format(DateTimeImmutable::ATOM);
        }

        /** @var \BulkAPIResponse $response */
        $response = $handler->getRecords(null, $sortByField, $sortDirection, $page, $perPage, $headers);

        /** @var \ZCRMRecord[] $records */
        $records = $response->getData();

        return $records;
    }

    /**
     * @return ZCRMRecord[]
     */
    public function doSearchRecords(
        string $moduleName,
        ?array $criteria = null,
        ?string $email = null,
        ?string $phone = null,
        ?string $word = null,
        ?int $page = null,
        ?int $perPage = null
    ): array
    {
        $module = ZCRMModule::getInstance($moduleName);

        if (
            $criteria === null &&
            $email === null &&
            $phone === null &&
            $word === null
        )
        {
            throw new \RuntimeException('There must be at least on search parameter filled.');
        }

        // order of parameters applied: criteria, email, phone and word
        if ($criteria !== null) {
            $response = $module->searchRecordsByCriteria($criteria, $page, $perPage);
        } elseif ($email !== null) {
            $response = $module->searchRecordsByEmail($email, $page, $perPage);
        } elseif ($phone !== null) {
            $response = $module->searchRecordsByPhone($phone, $page, $perPage);
        } else {
            // $word !== null
            $response = $module->searchRecords($word, $page, $perPage);
        }

        /** @var \ZCRMRecord[] $records */
        $records = $response->getData();

        return $records;
    }

    /* factory methods */
    public function getRecords(): GetRecordsRequest
    {
        return new GetRecordsRequest($this, $this->module);
    }

    public function getRecord(string $recordId): GetRecordRequest
    {
        return new GetRecordRequest($this, $this->module, $recordId);
    }

    public function searchRecords(): SearchRecordsRequest
    {
        return new SearchRecordsRequest($this, $this->module);
    }

    public function createRecord(\ZCRMRecord $record): CreateRecordRequest
    {
        return new CreateRecordRequest($this, $record);
    }

    public function updateRecord(\ZCRMRecord $record): UpdateRecordRequest
    {
        return new UpdateRecordRequest($this, $record);
    }

    public function deleteRecord(string $recordId): DeleteRecordRequest
    {
        return new DeleteRecordRequest($this, $this->module, $recordId);
    }

    /**
     * @param \ZCRMRecord[] $records
     */
    public function createRecords(array $records): CreateRecordsRequest
    {
        return new CreateRecordsRequest($this, $this->module, $records);
    }

    /**
     * @param \ZCRMRecord[] $records
     */
    public function updateRecords(array $records): UpdateRecordsRequest
    {
        return new UpdateRecordsRequest($this, $this->module, $records);
    }

    /**
     * @param string[] $recordIds
     */
    public function deleteRecords(array $recordIds): DeleteRecordsRequest
    {
        return new DeleteRecordsRequest($this, $this->module, $recordIds);
    }

}
