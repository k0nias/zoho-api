<?php declare(strict_types = 1);

namespace K0nias\ZohoApi;

class Configuration
{

    /**
     * @var string
     */
    private $pathToConfiguration;

    public function __construct(string $pathToConfiguration)
    {
        $this->pathToConfiguration = $pathToConfiguration;
    }

    /**
     * @see ROOT/configuration.properties.dist
     * @see ROOT/oauth_configuration.properties.dist
     *
     * @return string[]
     */
    public function loadToArray(): array
    {
        $configurationPath = $this->pathToConfiguration . '/configuration.properties';
        $oauthConfigurationPath = $this->pathToConfiguration . '/oauth_configuration.properties';

        $configuration = @parse_ini_file($configurationPath);
        $oauthConfiguration = @parse_ini_file($oauthConfigurationPath);

        if ($configuration === false) {
            throw new \RuntimeException(sprintf('Missing or invalid configuration file "%s"', $configurationPath));
        }

        if ($oauthConfiguration === false) {
            throw new \RuntimeException(sprintf('Missing or invalid configuration file "%s"', $oauthConfigurationPath));
        }

        return array_merge($configuration, $oauthConfiguration);
    }

}
